package vn.tinylab.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "\"user\"")
@Setter
@Getter
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstname;
    private String role;
    private String lastname;
    private String email;
    private String password;

}