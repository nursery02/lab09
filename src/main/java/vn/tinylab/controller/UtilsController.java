package vn.tinylab.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.tinylab.service.EmailForgotPassword;

import javax.mail.MessagingException;
import java.io.IOException;

@RestController
@RequestMapping("/utils")
@AllArgsConstructor
public class UtilsController {
    private final EmailForgotPassword emailForgotPassword;

    @PostMapping("/sendEmail")
    public ResponseEntity<?> sendEmail() throws MessagingException, IOException {
        emailForgotPassword.sendEmail("Võ Đình Thiên", "www.google.com", "trandatitnlu@gmail.com");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = mapper.readTree("{\"Greeting\": \"Greetings from Spring Boot!\"}");
        return ResponseEntity.ok(json);
    }
}