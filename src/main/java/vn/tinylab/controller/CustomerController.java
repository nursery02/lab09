package vn.tinylab.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.tinylab.entity.AbstractUser;
import vn.tinylab.model.request.UserRequest;
import vn.tinylab.service.CustomerService;
import vn.tinylab.service.RegisterService;

import javax.management.BadAttributeValueExpException;

@RestController
@RequestMapping("/customers")
@Api(tags = "CustomerController")
public class CustomerController {
    private final RegisterService registerService;
    private final CustomerService customerService;

    public CustomerController(
            @Qualifier("registerCustomerServiceImpl") RegisterService registerService,
            CustomerService customerService
    ) {
        this.registerService = registerService;
        this.customerService = customerService;
    }

    @ApiOperation(value = "Create a customer")
    @PostMapping
    public ResponseEntity<AbstractUser> createCustomer(@RequestBody UserRequest request) {
        AbstractUser createdCustomer = registerService.createUser(request);
        return new ResponseEntity<>(createdCustomer, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Add credit card")
    @PutMapping("{id}/add-credit")
    public ResponseEntity<AbstractUser> addCredit(@PathVariable long id, @RequestParam double credit) throws BadAttributeValueExpException {
        AbstractUser createdCustomer = customerService.addCreditCard(id, credit);
        return new ResponseEntity<>(createdCustomer, HttpStatus.OK);
    }
}
