package vn.tinylab.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.tinylab.entity.Order;
import vn.tinylab.model.request.OrderRequest;
import vn.tinylab.service.OrderService;

import javax.management.BadAttributeValueExpException;

@RestController
@AllArgsConstructor
@RequestMapping("orders")
@Api(tags = "OrderController")
public class OrderController {
    private final OrderService orderService;

    @ApiOperation(value = "Order product")
    @PostMapping
    public ResponseEntity<Order> orders(@RequestBody OrderRequest request) throws BadAttributeValueExpException {
        Order createdOrder = orderService.orderProducts(request);
        return new ResponseEntity<>(createdOrder, HttpStatus.CREATED);
    }
}
