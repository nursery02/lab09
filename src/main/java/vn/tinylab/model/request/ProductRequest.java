package vn.tinylab.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ProductRequest {
    private String name;
    private Double price;
    private Long currentQuantity;
    private Long vendorId;
}
