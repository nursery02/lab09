package vn.tinylab.service;

import org.springframework.transaction.annotation.Transactional;
import vn.tinylab.entity.AbstractUser;
import vn.tinylab.model.request.UserRequest;

public interface RegisterService {
    @Transactional
    AbstractUser createUser(UserRequest request);
}
