package vn.tinylab.service;

import org.springframework.transaction.annotation.Transactional;
import vn.tinylab.entity.Customer;

import javax.management.BadAttributeValueExpException;

public interface CustomerService {
    @Transactional
    Customer addCreditCard(long userId, double credit) throws BadAttributeValueExpException;
}
