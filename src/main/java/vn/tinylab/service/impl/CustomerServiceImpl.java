package vn.tinylab.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.tinylab.entity.Customer;
import vn.tinylab.repo.CustomerRepository;
import vn.tinylab.service.CustomerService;

import javax.management.BadAttributeValueExpException;
import javax.persistence.EntityExistsException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    @Override
    public Customer addCreditCard(long customerId, double credit) throws BadAttributeValueExpException {
        Optional<Customer> localCustomerOptional = customerRepository.findById(customerId);
        if(!localCustomerOptional.isPresent()) {
            throw new EntityExistsException(String.format("Not found customer with ID = {}", customerId));
        }

        Customer localCustomer = localCustomerOptional.get();
        localCustomer.setCredit(localCustomer.getCredit() + credit);
        return customerRepository.save(localCustomer);
    }
}
