package vn.tinylab.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.tinylab.entity.CompositeKey;
import vn.tinylab.entity.Customer;
import vn.tinylab.entity.Order;
import vn.tinylab.entity.Product;
import vn.tinylab.model.request.OrderRequest;
import vn.tinylab.repo.CustomerRepository;
import vn.tinylab.repo.OrderRepository;
import vn.tinylab.repo.ProductRepository;
import vn.tinylab.service.OrderService;

import javax.management.BadAttributeValueExpException;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;

    @Override
    public Order orderProducts(OrderRequest request) throws BadAttributeValueExpException {
        Optional<Product> localProductOptional = productRepository.findById(request.getProductId());
        if(!localProductOptional.isPresent()) {
            throw new EntityNotFoundException(String.format("Not found product with ID = {} to update it.", request.getProductId()));
        }

        Product localProduct = localProductOptional.get();
        if(localProduct.getCurrentQuantity() == 0) {
            throw new EntityNotFoundException(String.format("Product with ID = {} is empty", request.getProductId()));
        }

        Optional<Customer> localCustomerOptional = customerRepository.findById(request.getCustomerId());
        if(!localCustomerOptional.isPresent()) {
            throw new EntityExistsException(String.format("Not found customer with ID = {}", request.getCustomerId()));
        }

        // Rollback when not enough money
        Customer localCustomer = localCustomerOptional.get();
        if(localCustomer.getCredit() < localProduct.getPrice() * request.getQuantity()) {
            throw new BadAttributeValueExpException("Don't have enough money to buy this product");
        }

        if(localProduct.getVendor().getEnabled() == 0) {
            throw new BadAttributeValueExpException("Vendor is disabled.");
        }

        Order order = new Order();
        CompositeKey com = new CompositeKey();
        com.setProductId(request.getProductId());
        com.setCustomerId(request.getCustomerId());
        order.setCompositeKey(com);

        order.setQuantity(request.getQuantity());
        order.setTotalPrice(localProduct.getPrice() * request.getQuantity());
        order = orderRepository.save(order);

        return order;
    }
}
