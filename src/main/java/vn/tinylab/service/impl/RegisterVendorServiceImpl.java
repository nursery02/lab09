package vn.tinylab.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.tinylab.entity.AbstractUser;
import vn.tinylab.entity.Vendor;
import vn.tinylab.entity.constant.StatusConstant;
import vn.tinylab.model.request.UserRequest;
import vn.tinylab.repo.VendorRepository;
import vn.tinylab.service.RegisterService;

@Service
@AllArgsConstructor
public class RegisterVendorServiceImpl implements RegisterService {
    private final VendorRepository vendorRepository;

    @Override
    public AbstractUser createUser(UserRequest request) {
        request.setName(request.getName() == null ? "" : request.getName().trim());
        request.setCredit(request.getCredit() == null ? 0.0 : request.getCredit());

        Vendor createdVendor = new Vendor();
        createdVendor.setName(request.getName());
        createdVendor.setCredit(request.getCredit());
        createdVendor.setEnabled(StatusConstant.ENABLED);
        createdVendor = vendorRepository.save(createdVendor);

        return createdVendor;
    }
}
