package vn.tinylab.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.tinylab.entity.Vendor;

import java.util.Optional;

public interface VendorRepository extends JpaRepository<Vendor, Long> {
    Optional<Vendor> findById(long id);
}
