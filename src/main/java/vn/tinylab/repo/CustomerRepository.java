package vn.tinylab.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.tinylab.entity.Customer;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByName(String name);
}
