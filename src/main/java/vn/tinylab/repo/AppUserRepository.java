package vn.tinylab.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.tinylab.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    List<AppUser> findByEmail(String Email);
}